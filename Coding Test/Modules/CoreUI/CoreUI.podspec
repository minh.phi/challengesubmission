#
# Be sure to run `pod lib lint CoreUI.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'CoreUI'
  s.version          = '0.1.0'
  s.summary          = 'A short description of CoreUI.'
  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC
  s.homepage         = 'https://github.com/Long Vo/CoreUI'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Long Vo' => 'long.vo@savvycomsoftware.com' }
  s.source           = { :git => 'https://github.com/Long Vo/CoreUI.git', :tag => s.version.to_s }
  s.ios.deployment_target = '12.0'
  s.source_files = 'CoreUI/Classes/**/*'

  s.dependency 'Core'
end
