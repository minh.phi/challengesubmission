# Constant

[![CI Status](https://img.shields.io/travis/Long Vo/Constant.svg?style=flat)](https://travis-ci.org/Long Vo/Constant)
[![Version](https://img.shields.io/cocoapods/v/Constant.svg?style=flat)](https://cocoapods.org/pods/Constant)
[![License](https://img.shields.io/cocoapods/l/Constant.svg?style=flat)](https://cocoapods.org/pods/Constant)
[![Platform](https://img.shields.io/cocoapods/p/Constant.svg?style=flat)](https://cocoapods.org/pods/Constant)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Constant is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'Constant'
```

## Author

Long Vo, long.vo@savvycomsoftware.com

## License

Constant is available under the MIT license. See the LICENSE file for more info.
