#
# Be sure to run `pod lib lint News.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'News'
  s.version          = '0.1.0'
  s.summary          = 'A short description of News.'
  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC
  s.homepage         = 'https://github.com/Long Vo/News'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Long Vo' => 'long.vo@savvycomsoftware.com' }
  s.source           = { :git => 'https://github.com/Long Vo/News.git', :tag => s.version.to_s }
  s.ios.deployment_target = '12.0'
  s.source_files = 'News/**/**/*.{xib,swift,h,m}'
  
  s.resource_bundles = {
    'News' => ['News/Assets/*.{xcassets}']
  }
  
  s.test_spec 'Tests' do |test_spec|
    test_spec.source_files = 'News/Tests/**/*.{xib,swift,h,m}'
  end

  s.weak_framework = 'XCTest'
  s.dependency 'Support'
  s.dependency 'CoreUI'
  s.dependency 'Core'
  s.dependency 'Reusable'
  s.dependency 'SkeletonView'
  s.dependency 'Reusable'
  s.dependency 'SDWebImage', '~> 5.0'
end
