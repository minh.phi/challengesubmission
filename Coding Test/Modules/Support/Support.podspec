#
# Be sure to run `pod lib lint Support.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Support'
  s.version          = '0.1.0'
  s.summary          = 'A short description of Support.'
  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC
  s.homepage         = 'https://github.com/Long Vo/Support'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Long Vo' => 'long.vo@savvycomsoftware.com' }
  s.source           = { :git => 'https://github.com/Long Vo/Support.git', :tag => s.version.to_s }
  s.ios.deployment_target = '12.0'
  s.source_files = 'Support/**/**/*'
  
  s.test_spec 'Tests' do |test_spec|
    test_spec.source_files = 'Support/**/**/*'
  end

  s.weak_framework = 'XCTest'
  s.dependency 'Alamofire', '~> 4.7'
  s.dependency 'Constant'
  s.dependency 'RxSwift'
  s.dependency 'RxCocoa'
end
